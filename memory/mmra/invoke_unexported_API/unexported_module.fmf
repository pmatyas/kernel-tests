summary: Test case that verifies it is not possible for a kernel mode 
    driver to invoke an API that is not part of the exported symbol list for the kernel 
description: |
    Create a kernel module that attempts to  access an unexported variable.
    During compile time, linking against the unexported symbol should fail.
    If it is then forcefully linked or linked accidently then the test can try to load the module
    It would then fail with an error message for unresolved symbols.
    Test input:
        unexported_module.c, the module source.
        make test 2> test.log, trigger
        rlAssertGrep "ERROR: modpost: \"unexported_kernel_symbol\"" test.log
        insmod unexported_module.ko 2> test.log
        rlAssertGrep "ERROR: could not load module unexported_module.ko: No such file or directory" test.log
    Expected results:
        [   PASS   ] :: File 'test.log' should contain 'ERROR: modpost: "unexported_kernel_symbol"'
        [   PASS   ] :: File 'test.log' should contain 'ERROR: could not load module unexported_module.ko: No such file or directory'
    Results location:
        output.txt
contact: George Jacob <gjacob@redhat.com>
test: bash ./runtest.sh
framework: beakerlib
require:
  - beakerlib
  - make
  - type: file
    pattern:
        - /kernel-include
enabled: true
duration: 10m
id: 1cc89932-b982-47b2-b8ae-481b5120c8b6
